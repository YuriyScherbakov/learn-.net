﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InetCalcMVC.Models
{
    public enum Operations
    {
        plus,
        minus,
        multiply,
        divide,
        equals

    }
    [Serializable]
    public class CalcOperationModel
    {
      public  double firstNumber { get; set; }
      public double secondNumber { get; set; }
      public Operations operation { get; set; }
      public double result { get; set; }
        public CalcOperationModel() {; }

     public   CalcOperationModel (CalcOperationModel precedingModel = null, double currentNumber = 0, Operations currentOperation = Operations.plus)
        {
            this.firstNumber = precedingModel.Equals(null) ? 0 : precedingModel.secondNumber;
            this.secondNumber = currentNumber;
            this.operation = currentOperation;

            this.result = ResultExec(firstNumber, secondNumber, currentOperation);

            
        }

        private double ResultExec(double firstNumber, double secondNumber, Operations currentOperation)
        {
            switch (currentOperation)
            {
                case Operations.plus:
                    return firstNumber + secondNumber;
                case Operations.minus:
                    return firstNumber - secondNumber;
                case Operations.multiply:
                    return firstNumber * secondNumber;
                default:
                    return firstNumber / secondNumber;
                    
            }

        }
    }
}
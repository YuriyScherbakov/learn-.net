﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InetCalcMVC.Models;

namespace InetCalcMVC.Controllers
{
    public class myCookies
    {
       public HttpCookie Model;
       

        public myCookies(string firstNumber, string secondNumber, string operation, string result)
        {
            this.Model = new HttpCookie("Model");
            this.Model["firstNumber"] = firstNumber;
            this.Model["secondNumber"] = secondNumber;
            this.Model["operation"] = operation;
            this.Model["result"] = result;

        }


    }
    public class DefaultController : Controller
    {
        // GET: Default
        [HttpGet]
        public ActionResult Index(Operations operation = Operations.plus, CalcOperationModel modelIN = null , double currentNumber = 0)
        {

           
            CalcOperationModel model = new CalcOperationModel(modelIN, currentNumber, operation);

            myCookies modelCookies = new myCookies(model.firstNumber.ToString(), model.secondNumber.ToString(), model.operation.ToString(), model.result.ToString());
           
          HttpContext.Response.Cookies["Model"].Value = modelCookies.Model.Value;
          Response.Write(HttpContext.Response.Cookies["Model"].Value);
            return View(model);
        }
        [HttpPost]
        public ActionResult Index(FormCollection formCollection)
        {
            Operations operation = Operations.plus;
            Response.Write(formCollection["screen"]);
            Response.Write(formCollection["operation"]);

            if (!(formCollection["operation"] == String.Empty))
            {
                switch (formCollection["operation"])
                {
                    case "plus":
                        operation = Operations.plus; break;
                    case "minus":
                        operation = Operations.minus; break;
                    case "divide":
                        operation = Operations.divide; break;

                    case "multiply":
                        operation = Operations.multiply; break;

                    case "equals":
                        operation = Operations.equals; break;


                }
            }

            Response.Write(operation);

         //   CalcOperationModel model = new CalcOperationModel((CalcOperationModel)HttpContext.Request.Cookies["Model"], Double.Parse(formCollection["screen"]), operation);


            return View(/*model*/);
        }
    }
}